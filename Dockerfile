FROM alpine:latest

RUN apk -U upgrade --no-cache \
  && apk add --no-cache \
    bash \
    curl \
    jq \
    file \
  && rm -rf /var/cache/* \
  && rm -rf /root/.cache/*

COPY ./scripts /scripts

# end

